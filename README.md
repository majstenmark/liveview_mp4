# Liveview DEMO
## Overview
This is a demo of the 7-camera liveview developed in the project Surgeon's perspective. Runs on Ubuntu 20.04. 


### Software Installation
- `./install.sh` installs:
    + python3 + pip3
    + opencv (python dependency)
    + numpy (python dependency)
    + Ximea-api
    + openjdk-11
- install.sh assumes `apt` as package manager (default in Ubuntu). Should be modifiable to work for other linux distrubutions and macOS.
- The installation creates a desktop icon for Liveview.

### Running the demo
- Place seven mp4 videos in the folder mock-stream. 
- Start the script with `./run.sh`